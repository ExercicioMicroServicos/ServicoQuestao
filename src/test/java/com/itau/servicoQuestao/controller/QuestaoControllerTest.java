package com.itau.servicoQuestao.controller;

import org.junit.Test;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.util.ArrayList;
import java.util.Optional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itau.servicoQuestao.controller.RespostaController;
import com.itau.servicoQuestao.model.Questao;
import com.itau.servicoQuestao.model.Resposta;
import com.itau.servicoQuestao.repositories.QuestaoRepository;
import com.itau.servicoQuestao.repositories.RespostaRepository;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@WebMvcTest(QuestaoController.class)
public class QuestaoControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	QuestaoRepository questaoRepository;	
	
	ObjectMapper mapper = new ObjectMapper();

	@Test
	public void DeveRetornarQuestoes() throws Exception{
		Questao questao = new Questao();
		
		questao.setId(1);
		questao.setPergunta("Voce trabalha?");
		questao.setRespostas(null);

		ArrayList<Questao> questoes = new ArrayList<Questao>();
		questoes.add(questao);
		
		when(questaoRepository.findAll()).thenReturn(questoes);
		
		mockMvc.perform(get("/questoes"))
		.andExpect(jsonPath("$[0].id", equalTo(1)))
		.andExpect(jsonPath("$[0].pergunta", equalTo("Voce trabalha?")));
	}
	
	@Test
	public void DeveRetornarQuestoesPorQuantidade() throws Exception{
		Questao questao = new Questao();
		
		questao.setId(1);
		questao.setPergunta("Voce trabalha?");
		questao.setRespostas(null);

		ArrayList<Questao> questoes = new ArrayList<Questao>();
		questoes.add(questao);
		
		when(questaoRepository.findAll()).thenReturn(questoes);
		
		mockMvc.perform(get("/questoes/1"))
		.andExpect(jsonPath("$[0].id", equalTo(1)))
		.andExpect(jsonPath("$[0].pergunta", equalTo("Voce trabalha?")));
	}
	
	@Test
	public void DeveRetornarQuestaoUm() throws Exception{
		Questao questao = new Questao();
		
		questao.setId(1);
		questao.setPergunta("Voce trabalha?");
		questao.setRespostas(null);
		
		Optional<Questao> questaoOptional = Optional.of(questao);

		
		when(questaoRepository.findById(questao.getId())).thenReturn(questaoOptional);
		
		mockMvc.perform(get("/questao/1"))
		.andExpect(jsonPath("$.id", equalTo(1)))
		.andExpect(jsonPath("$.pergunta", equalTo("Voce trabalha?")));
	}
	
	@Test
	public void DeveRetornarQuestaoInexistente() throws Exception{
		Questao questao = new Questao();
		questao.setId(1);
		
		Optional<Questao> questaoOptional = Optional.empty();

		
		when(questaoRepository.findById(questao.getId())).thenReturn(questaoOptional);
		
		mockMvc.perform(get("/questao/1"))
		 .andExpect(MockMvcResultMatchers.status().isNotFound());
	}
	
	
	@Test
	public void DeveRetornarQuestaoCriada() throws Exception{
		
		Questao questao = new Questao();
		
		questao.setId(1);
		questao.setPergunta("Voce trabalha?");
		questao.setRespostas(null);
		
		when(questaoRepository.save(any(Questao.class))).thenReturn(questao);
		
		String json = mapper.writeValueAsString(questao);

		mockMvc.perform(post("/questao")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.id", equalTo(1)))
		.andExpect(jsonPath("$.pergunta", equalTo("Voce trabalha?")));
	}	
	
	@Test
	public void DeveDeletarQuestao() throws Exception{
		Questao questao = new Questao();
		
		questao.setId(1);
		questao.setPergunta("Voce trabalha?");
		questao.setRespostas(null);

		Optional<Questao> questaoOptional = Optional.of(questao);
				
		when(questaoRepository.findById(questao.getId())).thenReturn(questaoOptional);
		questaoRepository.deleteById(questao.getId());
		
		mockMvc.perform(delete("/questao/1"))
		 .andExpect(MockMvcResultMatchers.status().isOk());
	}
	
	@Test
	public void DeveDeletarQuestaoInexistente() throws Exception{
		Questao questao = new Questao();
		questao.setId(1);

		Optional<Questao> questaoOptional = Optional.empty();
				
		when(questaoRepository.findById(questao.getId())).thenReturn(questaoOptional);
		
		mockMvc.perform(delete("/questao/1"))
		 .andExpect(MockMvcResultMatchers.status().isNotFound());
	}
	
	@Test
	public void DeveAlterarQuestao() throws Exception{
		
		Questao questao = new Questao();
		
		questao.setId(1);
		questao.setPergunta("Voce trabalha?");
		questao.setRespostas(null);
		
		Optional<Questao> questaoOptional = Optional.of(questao);
				
		when(questaoRepository.findById(questao.getId())).thenReturn(questaoOptional);
		when(questaoRepository.save(any(Questao.class))).thenReturn(questao);
		
		String json = mapper.writeValueAsString(questao);

		mockMvc.perform(put("/questao/1")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.id", equalTo(1)))
		.andExpect(jsonPath("$.pergunta", equalTo("Voce trabalha?")));
	}
	
//	@Test
//	public void DeveAlterarQuestaoInexistente() throws Exception{
//		Questao questao = new Questao();
//		questao.setId(1);
//		
//		Optional<Questao> questaoOptional = Optional.empty();
//
//		when(questaoRepository.findById(questao.getId())).thenReturn(questaoOptional);
//		
//		mockMvc.perform(put("/questao/1"))
//		 .andExpect(MockMvcResultMatchers.status().isNotFound());
//	}
}
