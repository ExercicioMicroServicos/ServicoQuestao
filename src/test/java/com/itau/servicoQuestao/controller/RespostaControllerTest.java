package com.itau.servicoQuestao.controller;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itau.servicoQuestao.controller.RespostaController;
import com.itau.servicoQuestao.model.Questao;
import com.itau.servicoQuestao.model.Resposta;
import com.itau.servicoQuestao.repositories.RespostaRepository;
import java.util.ArrayList;
import java.util.Optional;

@RunWith(SpringRunner.class)
@WebMvcTest(RespostaController.class)
public class RespostaControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	RespostaRepository respostaRepository;	
	
	ObjectMapper mapper = new ObjectMapper();

	@Test
	public void buscarRespostas() throws Exception{
		Resposta r = new Resposta();
		
		r.setId(1);
		r.setDescricao("Pedro");
		r.setCorreta(true);

		ArrayList<Resposta> resposta = new ArrayList<Resposta>();
		resposta.add(r);
		
		when(respostaRepository.findAll()).thenReturn(resposta);
		
		mockMvc.perform(get("/respostas"))
		.andExpect(jsonPath("$[0].id", equalTo(1)))
		.andExpect(jsonPath("$[0].descricao", equalTo("Pedro")))
		.andExpect(jsonPath("$[0].correta", equalTo(true)));
	}
	
	@Test
	public void DeveRetornarRespostaInexistente() throws Exception{
		Resposta r = new Resposta();		
		r.setId(1);
		
		Optional<Resposta> respostaOptional = Optional.empty();

		
		when(respostaRepository.findById(r.getId())).thenReturn(respostaOptional);
		
		mockMvc.perform(get("/resposta/1"))
		 .andExpect(MockMvcResultMatchers.status().isNotFound());
	}
	
	@Test
	public void testarCriarResposta() throws Exception{
		
		Resposta resposta = new Resposta();
		
		resposta.setId(1);
		resposta.setDescricao("Pedro");
		resposta.setCorreta(true);
		
		when(respostaRepository.save(any(Resposta.class))).thenReturn(resposta);
		
		String json = mapper.writeValueAsString(resposta);

		mockMvc.perform(post("/resposta")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.id", equalTo(1)))
		.andExpect(jsonPath("$.descricao", equalTo("Pedro")))
		.andExpect(jsonPath("$.correta", equalTo(true)));
	}	
	
	@Test
	public void buscarResposta() throws Exception{

		Resposta resposta = new Resposta();

		resposta.setId(1);
		resposta.setDescricao("Pedro");
		resposta.setCorreta(true);
		
		Optional<Resposta> r = Optional.of(resposta);
			
		when(respostaRepository.findById((long) 1)).thenReturn(r);
		
		mockMvc.perform(get("/resposta/1"))
		.andExpect(jsonPath("$.id", equalTo(1)))
		.andExpect(jsonPath("$.descricao", equalTo("Pedro")))
		.andExpect(jsonPath("$.correta", equalTo(true)));
	}
	
	@Test
	public void deleteResposta() throws Exception{

		Resposta resposta = new Resposta();
		resposta.setId(1);
		resposta.setDescricao("Pedro");
		resposta.setCorreta(true);
		
		Optional<Resposta> r = Optional.of(resposta);
			
		when(respostaRepository.findById(resposta.getId())).thenReturn(r);
		respostaRepository.deleteById(resposta.getId());

		mockMvc.perform(delete("/resposta/1"))
		.andExpect(MockMvcResultMatchers.status().isOk());
		
	}
	
	@Test
	public void DeveDeletarRespostaInexistente() throws Exception{
		Resposta resposta = new Resposta();
		resposta.setId(1);

		Optional<Resposta> respostaOptional = Optional.empty();
				
		when(respostaRepository.findById(resposta.getId())).thenReturn(respostaOptional);
		
		mockMvc.perform(delete("/resposta/1"))
		 .andExpect(MockMvcResultMatchers.status().isNotFound());
	}
	
	@Test
	public void putResposta() throws Exception{

		Resposta resposta = new Resposta();

		resposta.setId(1);
		resposta.setDescricao("Pedro");
		resposta.setCorreta(true);
		
		Optional<Resposta> r = Optional.of(resposta);
		
		Resposta respostaBanco = r.get();
		respostaBanco.setId(resposta.getId());
		respostaBanco.setDescricao(resposta.getDescricao());
		respostaBanco.setCorreta(resposta.isCorreta());
		
		when(respostaRepository.findById(resposta.getId())).thenReturn(r);		
		when(respostaRepository.save(respostaBanco)).thenReturn(respostaBanco);	

		String json = mapper.writeValueAsString(respostaBanco);
		
		mockMvc.perform(put("/resposta/1")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.id", equalTo(1)))
		.andExpect(jsonPath("$.descricao", equalTo("Pedro")))
		.andExpect(jsonPath("$.correta", equalTo(true)));
	}
	
	
}
