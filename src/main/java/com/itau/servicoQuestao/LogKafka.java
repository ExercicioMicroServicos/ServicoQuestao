package com.itau.servicoQuestao;

import java.util.Properties;
import java.util.Random;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringSerializer;

public class LogKafka {
	
	private String topico;
	private String servers = "10.162.107.229:9090;10.162.107.229:9091;10.162.107.229:9092";

	private String texto;
	
	public LogKafka(String topico, String texto) {
		this.topico = topico;
		this.texto = texto;
	}

	public KafkaProducer<Long, String> createProducer(int index) {
		Properties props = new Properties();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, servers);
		props.put(ProducerConfig.CLIENT_ID_CONFIG, "Produtor-" + index);
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class);
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);

		KafkaProducer<Long, String> producer = new KafkaProducer<>(props);
		return producer;
	}

	public Thread createProducerThread(final int index) {
		Thread t = new Thread(new Runnable() {

			@Override
			public void run() {
				System.out.printf("Thread [%s] starting.\n", Thread.currentThread().getName());
				Random random = new Random();
				KafkaProducer<Long, String> producer = createProducer(index);
			
				final long key = random.nextInt(10000000);
				
				ProducerRecord<Long, String> record = new ProducerRecord<>(topico, key, texto);
				
				try {
					producer.send(record).get();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				producer.flush();
				producer.close();
			}
		});
		t.setName("ProducerThread-" + index);
		return t;
	}
}