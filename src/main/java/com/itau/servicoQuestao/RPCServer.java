package com.itau.servicoQuestao;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.itau.servicoQuestao.repositories.QuestaoRepository;

@Component
public class RPCServer {

	private static String QUEUE_NAME = "e.queue.questions.id";
	private static String AMQ_SERVER = "tcp://amq.oramosmarcos.com:61616";
	
	@Autowired
	QuestaoRepository questaoRepository;

	@Bean
	public boolean consomeFila() {
		try {
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(AMQ_SERVER);
			Connection connection = connectionFactory.createConnection();
			connection.start();
			
        	// Criar uma sessão
        	Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        	
        	// A fila que queremos popular
        	Queue queue = session.createQueue(QUEUE_NAME);
        	
        	MessageConsumer consumer = session.createConsumer(queue);
        	consumer.setMessageListener(new RequestListener(session, questaoRepository));
			
			
		} catch (JMSException ex) {
			ex.printStackTrace();
		}
		
		return true;
	}

}
