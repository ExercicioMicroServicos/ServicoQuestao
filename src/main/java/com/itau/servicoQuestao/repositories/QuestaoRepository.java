package com.itau.servicoQuestao.repositories;

import org.springframework.data.repository.CrudRepository;
import com.itau.servicoQuestao.model.Questao;

public interface QuestaoRepository extends CrudRepository<Questao, Long> {

}
