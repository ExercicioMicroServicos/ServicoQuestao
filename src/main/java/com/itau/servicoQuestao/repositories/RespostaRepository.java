package com.itau.servicoQuestao.repositories;

import org.springframework.data.repository.CrudRepository;
import com.itau.servicoQuestao.model.Resposta;

public interface RespostaRepository extends CrudRepository<Resposta, Long> {

}
