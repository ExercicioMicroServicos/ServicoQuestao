package com.itau.servicoQuestao.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.servicoQuestao.model.Resposta;
import com.itau.servicoQuestao.repositories.RespostaRepository;

@RestController
@CrossOrigin
public class RespostaController {
	
	@Autowired
	RespostaRepository respostaRepository;
	
	@RequestMapping(method=RequestMethod.POST, path="/resposta")
	public Resposta criarResposta(@Valid @RequestBody Resposta resposta) {
		return respostaRepository.save(resposta);
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/respostas")
	public Iterable<Resposta> buscarRespostas(){
		return respostaRepository.findAll();
	}
		
	@RequestMapping(method=RequestMethod.GET, path="/resposta/{id}")
	public ResponseEntity<?> buscarResposta(@PathVariable long id){
		
		Optional<Resposta> respostaOptional = respostaRepository.findById(id);
		
		if(!respostaOptional.isPresent()) {
			return  ResponseEntity.notFound().build();
		}
		
		return  ResponseEntity.ok().body(respostaOptional.get());
	}
	
	@RequestMapping(method=RequestMethod.PUT, path="/resposta/{id}")
	public ResponseEntity<?> alterarResposta(@Valid @PathVariable long id, @RequestBody Resposta resposta) {
		Optional<Resposta> respostaOptional = respostaRepository.findById(id);
		
		if(!respostaOptional.isPresent()) {
			return  ResponseEntity.notFound().build();
		}
		
		Resposta respostaBanco = respostaOptional.get();
		respostaBanco.setDescricao(resposta.getDescricao());
		respostaBanco.setCorreta(resposta.isCorreta());

		Resposta respostaSalva = respostaRepository.save(respostaBanco);
		
		return  ResponseEntity.ok().body(respostaSalva);	
	}
	
	@RequestMapping(method=RequestMethod.DELETE, path="/resposta/{id}")
	public ResponseEntity<?> deletarResposta(@PathVariable long id){
		
		Optional<Resposta> respostaOptional = respostaRepository.findById(id);
		
		if(!respostaOptional.isPresent()) {
			return  ResponseEntity.notFound().build();
		}
		
		respostaRepository.deleteById(id);
		
		return  ResponseEntity.ok().build();
	}

}
