package com.itau.servicoQuestao.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.servicoQuestao.model.Questao;
import com.itau.servicoQuestao.repositories.QuestaoRepository;

@RestController
@CrossOrigin
public class QuestaoController {
	
	@Autowired
	QuestaoRepository questaoRepository;
	
	@RequestMapping(method=RequestMethod.POST, path="/questao")
	public Questao criarQuestao(@Valid @RequestBody Questao questao) {
		return questaoRepository.save(questao);
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/questoes")
	public Iterable<Questao> buscarQuestoes(){
		return questaoRepository.findAll();
	}
		
	@RequestMapping(method=RequestMethod.GET, path="/questao/{id}")
	public ResponseEntity<?> buscarQuestao(@PathVariable long id){
		
		Optional<Questao> questaoOptional = questaoRepository.findById(id);
		
		if(!questaoOptional.isPresent()) {
			return  ResponseEntity.notFound().build();
		}
		
		return  ResponseEntity.ok().body(questaoOptional.get());
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/questoes/{quantidade}")
	public ArrayList<Questao> buscarQuestoesQuantidade(@PathVariable int quantidade){
		
		Iterator<Questao> questoes = questaoRepository.findAll().iterator();
		ArrayList<Questao> questoesSelecionadas = new ArrayList<Questao>();
		
		while(questoes.hasNext() && questoesSelecionadas.size() <= quantidade) {
			Questao questao = questoes.next();
			questoesSelecionadas.add(questao);
		}
		
		return questoesSelecionadas;
	}
	
	
	@RequestMapping(method=RequestMethod.PUT, path="/questao/{id}")
	public ResponseEntity<?> alterarQuestao(@Valid @PathVariable long id, @RequestBody Questao questao) {
		Optional<Questao> questaoOptional = questaoRepository.findById(id);
		
		if(!questaoOptional.isPresent()) {
			return  ResponseEntity.notFound().build();
		}
		
		Questao questaoBanco = questaoOptional.get();
		questaoBanco.setPergunta(questao.getPergunta());
		questaoBanco.setRespostas(questao.getRespostas());
		
		Questao questaoSalva = questaoRepository.save(questao);
		
		return  ResponseEntity.ok().body(questaoSalva);	
	}
	
	@RequestMapping(method=RequestMethod.DELETE, path="/questao/{id}")
	public ResponseEntity<?> deletarQuestao(@PathVariable long id){
		
		Optional<Questao> questaoOptional = questaoRepository.findById(id);
		
		if(!questaoOptional.isPresent()) {
			return  ResponseEntity.notFound().build();
		}
		
		questaoRepository.deleteById(id);
		
		return  ResponseEntity.ok().build();
	}

}
