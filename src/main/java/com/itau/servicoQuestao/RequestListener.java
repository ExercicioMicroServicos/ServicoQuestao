package com.itau.servicoQuestao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;

import com.itau.servicoQuestao.model.Questao;
import com.itau.servicoQuestao.repositories.QuestaoRepository;

public class RequestListener implements MessageListener {

	Session session;
	QuestaoRepository questaoRepository;

	public RequestListener(Session session, QuestaoRepository questaoRepository) {
		this.session = session;
		this.questaoRepository = questaoRepository;
	}

	public void onMessage(Message request) {
		MapMessage requestMap = (MapMessage) request;
		int quantidade;
		try {
			quantidade = Integer.parseInt(requestMap.getString("quantity"));

			Iterator<Questao> questoes = questaoRepository.findAll().iterator();
			ArrayList<Long> questoesSelecionadas = new ArrayList<Long>();
			
			while(questoes.hasNext() && questoesSelecionadas.size() <= quantidade) {
				Questao questao = questoes.next();
				questoesSelecionadas.add(questao.getId());
			}				
			
			MapMessage response = session.createMapMessage();
			response.setJMSCorrelationID(request.getJMSCorrelationID());
			response.setObject("ids", questoesSelecionadas);

			MessageProducer producer = session.createProducer(request.getJMSReplyTo());
			producer.send(response);

			producer.close();
		} catch (JMSException e) {
			e.printStackTrace();
		}

	}
}
